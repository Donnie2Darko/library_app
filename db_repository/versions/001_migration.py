from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
answers = Table('answers', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('body', String(length=140)),
    Column('question_id', Integer),
    Column('user_id', Integer),
)

votes = Table('votes', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('plus', Boolean, default=ColumnDefault(False)),
    Column('minus', Boolean, default=ColumnDefault(False)),
    Column('answer_id', Integer),
    Column('user_id', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['answers'].create()
    post_meta.tables['votes'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['answers'].drop()
    post_meta.tables['votes'].drop()
