__author__ = 'darko'

from flask.ext.wtf import Form
from wtforms import PasswordField, StringField, TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms.validators import InputRequired
from models import User, Question, Answer


class RegisterForm(Form):
    email = EmailField('email', validators=[InputRequired("Please enter your email address.")])
    nickname = StringField('nickname', validators=[InputRequired("Please enter your nickname.")])
    password = PasswordField('password', validators=[InputRequired("Please enter your password.")])

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        email = User.query.filter_by(email=self.email.data).first()
        if email is not None:
            self.email.errors.append('Email already registered')
            return False

        nickname = User.query.filter_by(nickname=self.nickname.data).first()
        if nickname is not None:
            self.nickname.errors.append('Nickname already registered')
            return False

        return True


class LoginForm(Form):
    DEFAULT_ERROR = 'Invalid Email or Password'

    email = EmailField('email', validators=[InputRequired("Please enter your email address.")])
    password = PasswordField('password', validators=[InputRequired("Please enter your password.")])
    user = None

    def validate(self):

        rv = Form.validate(self)
        if not rv:
            return False

        user = User.query.filter_by(email=self.email.data).first()
        if user is None:
            self.email.errors.append(self.DEFAULT_ERROR)
            return False

        if not user.check_password(self.password.data):
            self.email.errors.append(self.DEFAULT_ERROR)
            return False

        self.user = user
        return True


class QuestionForm(Form):
    DEFAULT_DUPLICATE_ERROR = 'Duplicate question'
    body = TextAreaField('body', validators=[InputRequired("Please enter text for your question.")])

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        if self.body.data == Question.query.filter_by(body=self.body.data).first():
            self.body.errors.append(self.DEFAULT_DUPLICATE_ERROR)
            return False

        questions = Question.query.all()
        if questions is None:
            return True

        user_body_words_list = self.body.data.split()
        for question in questions:
            db_body_words_list = question.body.split()
            if len(set(user_body_words_list).intersection(set(db_body_words_list)))*100/len(db_body_words_list) > 80:
                self.body.errors.append(self.DEFAULT_DUPLICATE_ERROR)
                return False

        return True


class AnswerForm(Form):
    DEFAULT_DUPLICATE_ERROR = 'Duplicate answer'
    body = TextAreaField('body', validators=[InputRequired("Please enter text for your answer.")])

    def __init__(self, question_id):
        super(AnswerForm, self).__init__()
        self._question_id = question_id

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        answers = Answer.query.filter_by(question_id=self._question_id)
        if answers is None:
            return True

        if self.body.data == Answer.query.filter_by(question_id=self._question_id,
                                                    body=self.body.data).first():
            self.body.errors.append(self.DEFAULT_DUPLICATE_ERROR)
            return False

        user_body_words_list = self.body.data.split()
        for answer in answers:
            db_body_words_list = answer.body.split()
            if len(set(user_body_words_list).intersection(set(db_body_words_list)))*100/len(db_body_words_list) > 80:
                self.body.errors.append(self.DEFAULT_DUPLICATE_ERROR)
                return False
        return True


class VoteForm(Form):
    pass


class DeleteForm(Form):
    pass