__author__ = 'darko'
import cgi
import hashlib
from library import db, login_manager
from flask.ext.login import UserMixin, current_user

class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), index=True, unique=True)
    nickname = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(120))

    def __init__(self, email, nickname, password):
        self.email = email
        self.nickname = nickname
        self.set_password(password)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def set_password(self, password):
        self.password = hashlib.md5(password).hexdigest()

    def check_password(self, password):
        return self.password == hashlib.md5(password).hexdigest()

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

    def __str__(self):
        return '{0}'.format(self.nickname)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Question(db.Model):
    __tablename__ = 'questions'

    def __init__(self, body):
        self.user_id = current_user.id
        self.body = cgi.escape(body)

    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    answers = db.relationship("Answer", backref='question', cascade="all, delete-orphan", lazy='dynamic')

    @property
    def author(self):
        return User.query.get(int(self.user_id))

    def __str__(self):
        return '{0}'.format(self.body)


class Answer(db.Model):
    __tablename__ = 'answers'

    def __init__(self, question_id, body):
        self.question_id = question_id
        self.user_id = current_user.id
        self.body = cgi.escape(body)

    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    question_id = db.Column(db.Integer, db.ForeignKey('questions.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    user = db.relationship("User", backref=db.backref('user'))
    votes = db.relationship("Vote", backref=db.backref('vote'))

    @property
    def pluses_sum(self):
        return sum([action.plus for action in self.votes])

    @property
    def minuses_sum(self):
        return sum([action.minus for action in self.votes])

    @property
    def voted_users(self):
        return [action.user for action in self.votes]

    def __str__(self):
        return '{0}'.format(self.body)


class Vote(db.Model):

    def __init__(self, answer_id):
        self.answer_id = answer_id
        self.user_id = current_user.id

    __tablename__ = 'votes'
    id = db.Column(db.Integer, primary_key=True)
    plus = db.Column(db.Boolean, default=False)
    minus = db.Column(db.Boolean, default=False)
    answer_id = db.Column(db.Integer, db.ForeignKey("answers.id"))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __str__(self):
        return "Vote for answer {answer_id}, plus: {plus}, minus: {minus}".format(self.__dict__)