from functools import wraps
from flask import render_template, redirect, url_for, request
from flask.ext.login import login_user, logout_user, current_user
from sqlalchemy.orm.exc import NoResultFound
from library import db, library_app
from library.forms import RegisterForm, LoginForm, QuestionForm, AnswerForm, VoteForm, DeleteForm
from library.models import User, Question, Answer, Vote


def can_post(func):
    @wraps(func)
    def wrapped_view(*args, **kwargs):
        if request.method == 'POST' and not current_user.is_authenticated:
            return "Unauthorized"
        return func(*args, **kwargs)
    return wraps(func)(wrapped_view)


@library_app.route('/', methods=['GET', 'POST'])
@library_app.route('/questions', methods=['GET', 'POST'])
@can_post
def questions():
    form = QuestionForm()
    if request.method == 'POST' and form.validate_on_submit():
        question = Question(form.body.data)
        db.session.add(question)
        db.session.commit()
    return render_template('questions.html',
                           title='Questions',
                           questions_data=Question.query.all(),
                           form=form,
                           form_delete=DeleteForm())


@library_app.route('/question/<question_id>', methods=['GET', 'POST'])
@can_post
def question_page(question_id):
    form = AnswerForm(question_id)
    if request.method == 'POST' and form.validate_on_submit():
        answer = Answer(question_id, form.body.data)
        db.session.add(answer)
        db.session.commit()
    try:
        question = Question.query.filter_by(id=question_id).one()
    except NoResultFound:
        return redirect_to_url(request)

    answers_data = sorted(Answer.query.filter_by(question_id=question.id),
                          key=lambda item: item.pluses_sum, reverse=True)
    answers_data = sorted(answers_data, key=lambda answer: answer.minuses_sum)  # sort on secondary key
    return render_template('question_answers.html',
                           title='Questions',
                           question=question,
                           answers_data=answers_data,
                           form_answer=form,
                           form_vote=VoteForm(),
                           form_delete=DeleteForm())


@library_app.route('/vote/<answer_id>', methods=['POST'])
@can_post
def vote_action(answer_id):
    form = VoteForm()
    if form.validate_on_submit():
        user_vote = Vote.query.filter_by(answer_id=answer_id, user_id=current_user.id).first()
        if not user_vote:
            user_vote = Vote(answer_id=answer_id)

        plus = True if request.form.get("vote") == "plus" else False
        user_vote.plus = plus
        user_vote.minus = not plus
        db.session.add(user_vote)
        db.session.commit()
    return redirect('/question/'+request.referrer.split('/')[-1])


@library_app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect_to_url(request)

    form = LoginForm()
    if request.method == 'POST' and form.validate_on_submit():
        login_user(form.user)
        return redirect_to_url(request)
    return render_template('login.html',
                           title='Sign In',
                           form=form)


@library_app.route('/logout')
def logout():
    if not current_user.is_authenticated:
        return redirect_to_url(request, "login")
    logout_user()
    return redirect(request.referrer)


@library_app.route('/register', methods=["GET", "POST"])
def register():
    form = RegisterForm()
    if request.method == 'POST' and form.validate_on_submit():
        user = User(email=form.email.data, nickname=form.nickname.data, password=form.password.data)
        db.session.add(user)
        db.session.commit()
        return redirect_to_url(request, "login")
    return render_template('register.html', form=form)


def redirect_to_url(request, default_url="questions"):
    url = url_for(default_url)
    if request.args.get("redirect_url"):
        url = request.args.get("redirect_url")
    return redirect(url)

@library_app.route('/delete/<table>/<res_id>', methods=["POST"])
@can_post
def delete_res(table, res_id):
    if table == "question":
        Question.query.filter_by(id=res_id).delete()
    elif table == "answer":
        Answer.query.filter_by(id=res_id).delete()
    db.session.commit()
    return redirect('/question/'+request.referrer.split('/')[-1])