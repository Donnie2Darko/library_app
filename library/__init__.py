from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager


library_app = Flask(__name__)
library_app.config.from_object('config')
db = SQLAlchemy(library_app)

login_manager = LoginManager()
login_manager.init_app(library_app)

from library import views, models