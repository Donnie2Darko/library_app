## Simple questions&answers project with registration. ##
# How to install: #

1. Clone project.
2. Create virtualenv with pip.
3. Install requirements: pip install -r requirements.txt
4. Create DataBase: python db_create.py
5. Apply migrations: python db_upgrade.py
6. Start server: python start_server.py
7. Open url for server an used it.
8. TEST 